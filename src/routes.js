import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Home from './pages/Home'
import Produtos from './pages/Produtos'
import ProdutosEditar from './pages/ProdutosEditar'
import Clientes from './pages/Clientes'
import ClientesEditar from './pages/ClientesEditar'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/produtos" component={Produtos} />
            <Route path="/produtos/novo" component={ProdutosEditar} />
            <Route path="/produtos/editar/:id" component={ProdutosEditar} />

            <Route exact path="/clientes" component={Clientes} />
            <Route path="/clientes/novo" component={ClientesEditar} />
            <Route path="/clientes/editar/:id" component={ClientesEditar} />
        </Switch>
    </BrowserRouter>
)

export default Routes