import React, { Component } from 'react';
import './App.css';

import Routes from './routes';
import Main from './pages/Main'

class App extends Component {
  render() {
    return (
      <div>
        <Main />
        <Routes />
      </div>
    );
  }
}

export default App;