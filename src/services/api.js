import axios from 'axios'

export const api = axios.create({
    baseURL: window.location.href.indexOf('localhost') > -1 ? 'http://localhost:4000/api' : 'https://minha-marmita-backend.herokuapp.com/api'
})

export const ioURL = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:4000' : 'https://minha-marmita-backend.herokuapp.com';