import React, { Component } from 'react'

import { Icon } from 'react-icons-kit'
import { ic_save } from 'react-icons-kit/md/ic_save'

import { api } from '../../services/api'

class ClientesEditar extends Component {

    state = {
        registro: {}
    }

    async componentDidMount() {
        const id = this.props.match.params.id;
        const res = await api.get('/clientes/' + id)

        this.setState({
            registro: res.data
        })
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const id = this.props.match.params.id;

        if (id) {
            await api.put('/clientes/' + this.state.registro._id, this.state.registro)
        } else {
            await api.post('/clientes', this.state.registro)
        }

        this.props.history.push('/clientes')
    }

    handleInputChange = (e) => {

        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Cliente</h1>
                </header>

                <form onSubmit={(e) => this.handleSalvar(e)}>
                    <p>
                        <input type="text" name="nome" value={this.state.registro.nome} onChange={this.handleInputChange} />
                    </p>

                    <p>
                        <input type="text" name="cpf" value={this.state.registro.cpf} onChange={this.handleInputChange} />
                    </p>

                    <p>
                        <input type="text" name="email" value={this.state.registro.email} onChange={this.handleInputChange} />
                    </p>

                    <button>
                        <Icon size={48} icon={ic_save} style={{ color: '#2A9D8F' }} />
                    </button>
                </form>

            </div>
        )
    }
}

export default ClientesEditar
