import React, { Component } from 'react'
import socket from 'socket.io-client'

import { Icon } from 'react-icons-kit'
import { ic_add } from 'react-icons-kit/md/ic_add'
import { ic_delete_forever } from 'react-icons-kit/md/ic_delete_forever'
import { ic_create } from 'react-icons-kit/md/ic_create'

import { api, ioURL } from '../../services/api'
import logo from '../../assets/logo.png'
import './styles.css'

class Produtos extends Component {

    state = {
        registros: []
    }

    async componentDidMount() {
        this.subscribeToNewFiles();

        const res = await api.get('/produtos')

        this.setState({
            registros: res.data.docs
        })
    }

    subscribeToNewFiles = () => {
        const io = socket(ioURL)

        io.on('novoProduto', data => {
            this.setState({
                registros: [data, ...this.state.registros]
            })
        })
    }

    handleNovo = () => {
        this.props.history.push('/produtos/novo/')
    }

    handleEditar = (i) => {
        this.props.history.push('/produtos/editar/' + i._id)
    }

    handleExcluir = async (i) => {
        this.setState({
            registros: [...this.state.registros.filter(item => { return item._id !== i._id })]
        })

        await api.delete('/produtos/' + i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <img src={logo} alt="" />
                    <h1>Produtos</h1>
                </header>

                <button onClick={() => this.handleNovo()}>
                    <Icon size={48} icon={ic_add} style={{ color: '#2A9D8F' }} />
                </button>

                <ul>
                    {this.state.registros && this.state.registros.map((item, i) => (
                        <li key={i}>
                            <a href={'/registros/' + item._id}>
                                <b>{item.titulo}</b>
                            </a>

                            <span>
                                <button onClick={() => this.handleEditar(item)}>
                                    <Icon size={24} icon={ic_create} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Icon size={24} icon={ic_delete_forever} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default Produtos
