import React, { Component } from 'react'
import './styles.css'

class Main extends Component {
    render() {
        return (
            <div id="main-container">
                <ul>
                    <li><a href="/">Principal</a></li>
                    <li><a href="/clientes">Clientes</a></li>
                    <li><a href="/produtos">Produtos</a></li>
                </ul>
            </div>
        )
    }
}

export default Main